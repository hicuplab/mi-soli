# MI-soli

This is a public repository for The Missing Interface project (MI-soli) where Google Soli sensors is used for gesture recognition on physical objects. This repository will store datasets and models that were used and are described in detail within our publication titled _**No Interface, No Problem: Gesture Recognition on Physical Objects Using Radar Sensing**_ 

Nuwan T. Attygalle, Luis A. Leiva, Matjaž Kljun, Christian Sandor, Alexander Plopski, Hirokazu Kato, and Klen Čopič Pucihar. 2021. No Interface, No Problem: Gesture Recognition on Physical Objects Using Radar Sensing. Sensors, (2021), no. 17: 5771, 20 pages. https://doi.org/10.3390/s21175771


**Other relevant publications:**


Klen Čopič Pucihar, Nuwan T. Attygalle, Matjaž Kljun, Christian Sandor, and Luis A. Leiva. 2022. Solids on
Soli: Millimetre-Wave Radar Sensing through Materials. Proc. ACM Hum.-Comput. Interact. , (2022), 20 pages.
https://doi.org/10.1145/3532212

Luis A. Leiva, Matjaz Kljun, Christian Sandor, and Klen Čopič Pucihar. 2020. The Wearable Radar: Sensing Gestures Through Fabrics. In 22nd International Conference on Human-Computer Interaction with Mobile Devices and Services (MobileHCI '20). Association for Computing Machinery, New York, NY, USA, Article 17, 1–4.https://doi.org/10.1145/3406324.3410720

Klen Čopič Pucihar, Christian Sandor, Matjaž Kljun, Wolfgang Huerst, Alexander Plopski, Takafumi Taketomi, Hirokazu Kato, and Luis A. Leiva. 2019. The Missing Interface: Micro-Gestures on Augmented Objects. In Extended Abstracts of the 2019 CHI Conference on Human Factors in Computing Systems (CHI EA '19). Association for Computing Machinery, New York, NY, USA, Paper LBW0153, 1–6.https://doi.org/10.1145/3290607.3312986


### Exporting the conda env

1. Install anaconda
2. Replicate our environment 
    ``` conda env create --file env.yml -n env_name ```

### Creating the dataset for per user evaluation of model architectures at varying dB ranges ([-2, 0], [-4, 0], [-8, 0], [-16, 0], [-32, 0] )

```
$ cd  ./DBRangeVSACCbyUser
$ # navigate to each directory {RD_2, RD_4, RD_8, RD_16, RD_32}
$ # concatenate the zip files into one file
$ # extract the zip files in the same directory as below.
$ 
$ # eg 1.
$ cd RD_2
$ cat RD_2.z* > all.zip
$ unzip all.zip -d ./
$
$ # eg 2.
$ cd RD_4
$ cat xa* > all.zip
$ unzip all.zip -d ./
```
### Creating the dataset of all users, 11 gestures at [-16,0] dB range. 

```
$ cd ./RD_16
$ cat x* > all.zip
$ unzip all.zip -d ./
```
Final folder structure is like below. 
